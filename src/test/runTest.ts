import * as path from 'path';

import { runTests } from 'vscode-test';
import { fstat } from 'fs';

async function main() {
  try {
    // The folder containing the Extension Manifest package.json
    // Passed to `--extensionDevelopmentPath`
    const extensionDevelopmentPath = path.resolve(__dirname, '../../');

    // The path to test runner
    // Passed to --extensionTestsPath
    const extensionTestsPath = path.resolve(__dirname, './suite/index');

    await runTests({ extensionDevelopmentPath, extensionTestsPath, launchArgs: [path.resolve(__dirname, '../../')]});
  } catch (err) {
    console.error('Failed to run tests');
    process.exit(1);
  }
}

main();
