import * as assert from 'assert';

import * as vscode from 'vscode';
import * as path from 'path';

suite('slipbox', () => {
	vscode.window.showInformationMessage('Start all tests.');

	test('Test permalink', async () => {
		const filePath = vscode.Uri.file(path.join(vscode.workspace.rootPath!, '/test/test-file.md'));
		const document = await vscode.workspace.openTextDocument(filePath);
		const editor = await vscode.window.showTextDocument(document);
		editor.selection = new vscode.Selection(new vscode.Position(2,0),new vscode.Position(4,0));
		const urls = new Array<String>();
		const originalExecute = vscode.commands.executeCommand;

		// TODO replace this hacked command interceptor with something nicer
		vscode.commands.executeCommand = (command, ...rest) => {
			if (command === 'vscode.open') {
				urls.push(rest[0]);
				return Promise.resolve(undefined);
			}else{
				return originalExecute(command, ...rest);
			}
		};
		await vscode.commands.executeCommand('slipbox.permalink');
		assert.deepEqual(urls[0].toString(), 'https://gitlab.com/viktomas/slipbox/-/blob/58ac9a6f7ac1472bb02dba4620fa1ba49a1a5619/test/test-file.md#L2-4');
	});
});
