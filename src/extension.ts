// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import WikiUriHandler from './wikiUriHandler';
import LinkCompletionProvider from './linkAutoCompletion';
import { generateId } from './idGenerator';
import { newPermanentNote, newReferenceNote } from './newNote';
import addFootnote from './footnoteCommand';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
  // Use the console to output diagnostic information (console.log) and errors (console.error)
  // This line of code will only be executed once when your extension is activated
  console.log('Congratulations, your extension "slipbox" is now active!');

  let disposable = vscode.commands.registerCommand('slipbox.insertId', async () => {
    const editor = vscode.window.activeTextEditor;
    if (!editor) {
      console.error('no open editor');
      return;
    }
    editor.edit((editBuilder) => {
      const now = new Date();
      editBuilder.insert(editor.selection.start, generateId(now));
    });
  });

  context.subscriptions.push(disposable);

  context.subscriptions.push(
    vscode.commands.registerCommand('slipbox.newPermanentNote', newPermanentNote),
  );

  context.subscriptions.push(
    vscode.commands.registerCommand('slipbox.newReferenceNote', newReferenceNote),
  );

  const selector: vscode.DocumentSelector = { language: 'markdown', scheme: '*' };

  let handlerDisposable = vscode.window.registerUriHandler(new WikiUriHandler());
  context.subscriptions.push(handlerDisposable);

  let providerDisposable = vscode.languages.registerCompletionItemProvider(
    'markdown',
    new LinkCompletionProvider(),
    '[',
  );
  context.subscriptions.push(providerDisposable);
  context.subscriptions.push(
    vscode.commands.registerCommand('slipbox.insertUrlFootnote', addFootnote),
  );
}

// this method is called when your extension is deactivated
export function deactivate() {}
