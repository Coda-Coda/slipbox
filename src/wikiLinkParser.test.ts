import parseWikiLinks from './wikiLinkParser';
import * as assert from 'assert';

describe('parseWikiLinks', () => {
  it('parses simple link', () => {
    const text = 'this [[link is]] going to get parsed';
    const result = parseWikiLinks(text);
    assert.deepStrictEqual(result, [{ start: 7, end: 14, content: 'link is' }]);
  });
});
