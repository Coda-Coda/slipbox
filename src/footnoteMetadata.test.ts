import { getFootnoteMetadata } from './footnoteMetadata';
import * as assert from 'assert';

describe('footnoteMetadata', () => {
  describe('getFootnoteMetadata', () => {
    it('gets page title from my blog', async () => {
      const { title } = await getFootnoteMetadata('https://element.io/blog/welcome-to-element/');
      assert.strictEqual(title, 'Welcome to Element!');
    });
  });
});
