import * as vscode from 'vscode';
import { generateId } from './idGenerator';
import * as path from 'path';
import { TextEncoder } from 'util';
import { assert } from 'console';

const lowerSnakeCase = (noteTitle: string): string => {
  return noteTitle
    .toLowerCase()
    .replace(/ /g, '-')
    .replace('--', '-')
    .replace(/[^\w-]/g, '');
};

const createAndOpenFile = async (folder: string, filename: string, content: string) => {
  assert(vscode.workspace.rootPath);
  const fullPath = vscode.Uri.parse(path.join(vscode.workspace.rootPath || '', folder, filename));
  await vscode.workspace.fs.writeFile(fullPath, new TextEncoder().encode(content));
  await vscode.commands.executeCommand('vscode.open', fullPath);
};

const noteTitleDialogue = async (): Promise<string | undefined> => {
  let question = `What's the note title?`;

  let selectedFilePath = await vscode.window.showInputBox({
    prompt: question,
  });

  return selectedFilePath;
};

export async function newPermanentNote() {
  if (!vscode.workspace.rootPath) {
    return;
  }

  const title = await noteTitleDialogue();
  if (!title) {
    return;
  }

  const filename = `${generateId()}-${lowerSnakeCase(title)}.md`;
  const permanentFolder = vscode.workspace.getConfiguration('slipbox').permanentFolder;
  await createAndOpenFile(permanentFolder, filename, `# ${title}\n\n`);
}

export async function newReferenceNote() {
  if (!vscode.workspace.rootPath) {
    return;
  }

  const title = await noteTitleDialogue();
  if (!title) {
    return;
  }

  const filename = `${lowerSnakeCase(title)}.md`;
  const permanentFolder = vscode.workspace.getConfiguration('slipbox').referenceFolder;
  await createAndOpenFile(permanentFolder, filename, `# ${title}\n\n`);
}
