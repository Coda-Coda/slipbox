export const generateId = (datetime: Date = new Date()) => {
  const pad = (num: number, size: number): string => {
    var s = num + '';
    while (s.length < size) {
      s = '0' + s;
    }
    return s;
  };
  // I didn't want to bring any external dependency yet.
  return `${datetime.getFullYear()}${pad(datetime.getMonth() + 1, 2)}${pad(
    datetime.getDate(),
    2,
  )}${pad(datetime.getHours(), 2)}${pad(datetime.getMinutes(), 2)}${pad(datetime.getSeconds(), 2)}`;
};
