import axios from 'axios';
import * as cheerio from 'cheerio';

// TODO type the parameter
export async function getFootnoteMetadata(url: string): Promise<{ title: string; url: string }> {
  const response = await axios.get(url);
  const parsedPage = cheerio.load(response.data);
  return { title: parsedPage('title').text(), url };
}
