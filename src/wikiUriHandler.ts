import * as vscode from 'vscode';

export default class WikiUriHandler implements vscode.UriHandler {
  async handleUri(uri: vscode.Uri): Promise<void> {
    const linkName = uri.path.replace('/', ''); // TODO only the first /
    const files = await vscode.workspace.findFiles('**/*' + linkName + '*');
    if (files.length > 0) {
      vscode.commands.executeCommand('vscode.open', files[0]);
    }
  }
}
