# Change Log

## 0.4.0 - *upcoming*

**Breaking change removing unused command, but still without 1.0.0 commitment**.

- New command to create footnote from URL (with automatic fetch of the page title)
- New command to create a reference note
- Removed insert ID command


## 0.3.0 - 2020-07-12

**Breaking change in autocompletion, but still without 1.0.0 commitment**.

- Autocompletion of markdown links

## 0.2.0 - 2020-06-18

- Command to create a new permanent note

## 0.1.1

- fix incorrect ID timestamp generating (was using day in the week)

## 0.1.0 - Initial release

- supports 3 basic operations
  - Generate timestamp ID
  - click through WikiLink
  - Autocomplete wikilinks