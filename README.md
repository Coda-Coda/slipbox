# slipbox

VS Code extension providing Zettelkasten support. I'm slowly getting making the extension a bit more opinionated.

**Warning**: this extension is in an early development. I don't see how it could do anything to your files, but please back up (version) your files.

## Features

### Command to create a new permanent note

Create a new note with "Slipbox: New Permanent Note" command.

![new note gif](https://gitlab.com/viktomas/slipbox/-/raw/images/img/new-note.gif)

### Command to create a new reference note

Create a new note with "Slipbox: New Reference Note" command.

TODO GIF

### Autocomplete links

Uses native VS Code symbol parsing for Markdown headings (the same as "Go to Symbol in Workspace" command). Uses H1 titles for link title and relative file to the MarkDown file as path.

![autocompletion gif](https://gitlab.com/viktomas/slipbox/-/raw/087ea0329211da1a02f693de1caa67e0f15d27eb/img/auto-completion.gif)

### Automatic footnotes from URL

Command: `Slipbox: Insert Footnote from URL`. You enter URL and this extension automatically generates `[^1]` style footnote and fetches title from the referenced site.

TODO GIF

## Goes well with

- [Markdown links](https://marketplace.visualstudio.com/items?itemName=tchayen.markdown-links)
- [Markdownlint](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint)
- [Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one)

## Extension Settings

- `slipbox.permanentFolder` - Path to the folder where you store permanent notes e.g. 'permanent'
  - New notes are created in the workspace root by default