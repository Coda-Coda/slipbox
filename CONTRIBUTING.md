## Automation

### Linting

Task: `yarn run lint`

Linting is done using `eslint` and `prettier`. Code can be formatted/fixed automatically by running `yarn run lint-fix`

### Unit Testing

Task: `yarn run test-unit`

Unit testing is done by `mocha` and `assert`. **Only files that are not importing `vscode` can be unit tested**.

The `*.text.ts` name format for both integration tests and unit tests is a bit confusing and potentially subject to change.

### Integration testing

Task: `yarn run test-integration`

These tests need an X server and vscode binary to run. More information is in the [official documentation on testing](https://code.visualstudio.com/api/working-with-extensions/testing-extension)

### Publishing

1. [Get a personal access token](https://code.visualstudio.com/api/working-with-extensions/publishing-extension#get-a-personal-access-token)
1. Add the token as `AZURE_ACCESS_TOKEN` [environment variable to your project CI](https://docs.gitlab.com/ee/ci/variables/#custom-environment-variables-of-type-variable)
1. Change extension version in `package.json` commit, push and tag the commit.
1. On tag pipelines, you can now manually trigger the publish step.
